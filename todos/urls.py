from django.urls import path

from todos.views import (
    TodoCreateView,
    TodoDeleteView,
    TodoDetailView,
    TodoUpdateView,
    TodoItemCreateView,
    TodoListView,

)

urlpatterns = [
    path("<int:pk>/", TodoDetailView.as_view(), name="todos_detail"),
    path("<int:pk/edit/", TodoUpdateView.as_view(), name="todos_edit"),
    path("int:pk/delete/", TodoDeleteView.as_view(), name="todos_delete"),
    path("create/", TodoCreateView.as_view(), name="todos_create"),
    path("createitem/", TodoItemCreateView.as_view(), name="todos_creatitem"),
    path("", TodoListView.as_view(), name="todos_list"),
]