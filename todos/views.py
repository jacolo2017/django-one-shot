from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy
# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.url"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

class TodoDeleteView(DeleteView):
    model=TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")

class TodoItemCreateView(CreateView):
    model= TodoItem
    template_name = "todos/createitem.html"
    fields = ["task", "due_date", "is_completed"]
    success_url = reverse_lazy("todos_detail")

